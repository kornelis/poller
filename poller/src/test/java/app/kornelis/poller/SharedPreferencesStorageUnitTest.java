package app.kornelis.poller;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.test.core.app.ApplicationProvider;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import app.kornelis.poller.data.SharedPreferencesStorage;

import static app.kornelis.poller.constants.States.LATER;
import static app.kornelis.poller.constants.States.NO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Unit Test for the Shared Preferences Storage.
 */
@Config(manifest=Config.NONE)
@RunWith(RobolectricTestRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SharedPreferencesStorageUnitTest {

    private static final int ZERO = 0;
    private static final int FIVE = 6;

    private final Context context = ApplicationProvider.getApplicationContext();
    private final SharedPreferencesStorage storage = new SharedPreferencesStorage(context);

    @Test
    public void a_storage_creation_test() {
        assertNotEquals(null, storage);
    }

    @Test
    public void b_storage_check_default_values() {
        assertEquals(ZERO, storage.getStatus());
        assertEquals(ZERO, storage.getDaysInstalled());
        assertEquals(ZERO, storage.getAskDayOffset());
        assertEquals(ZERO, storage.getTimesLaunched());
        assertEquals(ZERO, storage.getAskTimeOffset());
    }

    @Test
    public void c_storage_set_and_check_values() {
        storage.setStatus(LATER);
        storage.addAksDayOffset(FIVE);
        storage.setTimesLaunched(FIVE);
        storage.addAskTimeOffset(FIVE);

        assertNotEquals(NO, storage.getStatus());
        assertNotEquals(ZERO, storage.getStatus());
        assertNotEquals(ZERO, storage.getAskDayOffset());
        assertNotEquals(ZERO, storage.getTimesLaunched());
        assertNotEquals(ZERO, storage.getAskTimeOffset());

        assertEquals(LATER, storage.getStatus());
        assertEquals(ZERO, storage.getDaysInstalled());
        assertEquals(FIVE, storage.getTimesLaunched());
        assertEquals(FIVE, storage.getAskDayOffset());
        assertEquals(FIVE, storage.getAskTimeOffset());
    }

    @Test
    public void d_storage_mess_with_time_test() {
        // Set needed values to 0.
        storage.setTimesLaunched(ZERO);

        assertEquals(ZERO, storage.getDaysInstalled());
        assertEquals(ZERO, storage.getTimesLaunched());

        // Testing of different days in the storage.
        SharedPreferences sharedPreferences = context.getSharedPreferences("poll-preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("poll-times", FIVE);
        editor.apply();

        assertEquals(FIVE, storage.getTimesLaunched());
        assertNotEquals(ZERO, storage.getTimesLaunched());

        // And now the hardest part, times with days...
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date now = Calendar.getInstance().getTime();
        Date earlier = new Date(now.getTime() - (TimeUnit.DAYS.toMillis(FIVE)));
        // Storing
        editor.putString("poll-day", dateFormat.format(earlier));
        editor.apply();
        // Checking.
        assertEquals(FIVE, storage.getDaysInstalled());
        assertNotEquals(ZERO, storage.getDaysInstalled());
    }
}
