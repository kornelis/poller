package app.kornelis.poller;

import org.junit.Test;

import app.kornelis.poller.constants.States;
import app.kornelis.poller.constants.Trigger;
import app.kornelis.poller.model.State;
import static org.junit.Assert.assertEquals;

/**
 * Unit test for the state object(s).
 */
public class StateUnitTests {

    @Test
    public void state_creation_test() {
        State state = new State(Trigger.TIME_OR_LAUNCH, 3, 3)
                .setTrigger(Trigger.TIME)
                .setDelays(6, 6)
                .setStatus(States.LATER)
                .setPollDelayDay(4)
                .setPollAfterDay(11)
                .setPollDelayLaunchTimes(4)
                .setPollAfterLaunchTimes(12)
                .setDaysInstalled(5)
                .setTimesLaunched(5);

        assertEquals(state.trigger, Trigger.TIME);
        assertEquals(state.pollAfterDay, 11);
        assertEquals(state.pollAfterLaunchTimes, 12);
        assertEquals(state.status, States.LATER);
        assertEquals(state.pollDelayDay, 4);
        assertEquals(state.pollDelayLaunchTimes, 4);
        assertEquals(state.daysInstalled, 5);
        assertEquals(state.timesLaunched, 5);
    }
}
