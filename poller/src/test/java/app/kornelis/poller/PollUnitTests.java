package app.kornelis.poller;

import org.junit.Test;

import app.kornelis.poller.model.Poll;

import static org.junit.Assert.*;

/**
 * Unit test for the model object(s).
 */
public class PollUnitTests {

    @Test
    public void poll_builder_values_test() {
        String title = "title";
        String question = "question";
        String negative = "negative";
        String neutral = "neutral";
        String positive = "positive";

        Poll poll = new Poll.Builder(title)
                .setTitle(title)
                .setQuestion(question)
                .setOptionNegative(negative)
                .setOptionNeutral(neutral)
                .setOptionPositive(positive)
                .build();

        assertEquals(poll.title, title);
        assertEquals(poll.question, question);
        assertEquals(poll.optionNegative, negative);
        assertEquals(poll.optionNeutral, neutral);
        assertEquals(poll.optionPositive, positive);
    }
}