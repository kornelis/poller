package app.kornelis.poller;

/**
 * @author Kornelis.
 */
public interface PollFeedback {

    void onNegative();

    void onNeutral();

    void onPositive();

    void onCancel();
}
