package app.kornelis.poller;

/**
 * @author Kornelis.
 *
 * Polling the user for Feedback or a Review, using dialogs for the questions.
 */
public class Poller extends BasePoller {

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onNeutral() {

    }

    @Override
    public void onNegative() {

    }

    @Override
    public void onPositive() {

    }

    @Override
    public void onCancel() {

    }
}
