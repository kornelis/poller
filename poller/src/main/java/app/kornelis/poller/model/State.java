package app.kornelis.poller.model;

import static app.kornelis.poller.constants.States.*;
import static app.kornelis.poller.constants.Trigger.*;

/**
 * @author Kornelis.
 *
 * Keeping track of where we stand with our Poll.
 */
public class State {

    @TriggerOn
    public int trigger;
    @PollStates
    public int status = NOT_ASKED;
    public int pollAfterDay;
    public int pollDelayDay = 5;
    public int pollAfterLaunchTimes;
    public int pollDelayLaunchTimes = 4;
    public int daysInstalled = 0;
    public int timesLaunched = 0;

    public State() {
        this(TIME_AND_LAUNCH, 7, 5);
    }

    /**
     * Fast setting of often adjusted values.
     * @param trigger when the users gets to interact with the Poll.
     * @param pollAfterDay after how many days.
     * @param pollAfterLaunchTimes after how many times of app started.
     */
    public State(@TriggerOn int trigger, int pollAfterDay, int pollAfterLaunchTimes) {
        this.trigger = trigger;
        this.pollAfterDay = pollAfterDay;
        this.pollAfterLaunchTimes = pollAfterLaunchTimes;
    }

    /**
     * Set both delays with one function.
     * @param delayDays how many days we wait if the users answers later.
     * @param delayLaunchTimes how many launch times we add if the user answers later.
     * @return this
     */
    public State setDelays(int delayDays, int delayLaunchTimes) {
        pollDelayDay = delayDays;
        pollDelayLaunchTimes = delayLaunchTimes;
        return this;
    }

    public State setTrigger(@TriggerOn int trigger) {
        this.trigger = trigger;
        return this;
    }

    public State setStatus(@PollStates int status) {
        this.status = status;
        return this;
    }

    public State setPollAfterDay(int pollAfterDay) {
        this.pollAfterDay = pollAfterDay;
        return this;
    }

    public State setPollDelayDay(int pollDelayDay) {
        this.pollDelayDay = pollDelayDay;
        return this;
    }

    public State setPollAfterLaunchTimes(int pollAfterLaunchTimes) {
        this.pollAfterLaunchTimes = pollAfterLaunchTimes;
        return this;
    }

    public State setPollDelayLaunchTimes(int pollDelayLaunchTimes) {
        this.pollDelayLaunchTimes = pollDelayLaunchTimes;
        return this;
    }

    public State setDaysInstalled(int daysInstalled) {
        this.daysInstalled = daysInstalled;
        return this;
    }

    public State setTimesLaunched(int timesLaunched) {
        this.timesLaunched = timesLaunched;
        return this;
    }
}
