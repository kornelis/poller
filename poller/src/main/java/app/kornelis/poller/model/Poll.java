package app.kornelis.poller.model;

import androidx.annotation.NonNull;

/**
 * @author Kornelis.
 *
 * Create a Poll, single question, to show the user.
 */
public class Poll {

    public String title;
    public String question;
    public String optionNegative;
    public String optionNeutral;
    public String optionPositive;

    /**
     * Public constructor to help create a Poll.
     * @param builder with the values to show the user.
     */
    public Poll(@NonNull Builder builder) {
        title = builder.title;
        question = builder.question;
        optionNegative = builder.optionNegative;
        optionNeutral = builder.optionNeutral;
        optionPositive = builder.optionPositive;
    }

    /**
     * Inner Builder Class
     */
    public static class Builder {
        private String title;
        private String question;
        private String optionNegative;
        private String optionNeutral;
        private String optionPositive;

        /**
         * Start the builder and provide a title.
         * @param title of the Poll
         */
        public Builder(@NonNull String title) {
            this.title = title;
        }

        /**
         * Optional function to (re) set the title
         * @param title of the Poll
         * @return this builder
         */
        public Builder setTitle(@NonNull String title) {
            this.title = title;
            return this;
        }

        /**
         * Set the question of the Poll.
         * @param question of the Poll
         * @return this builder
         */
        public Builder setQuestion(@NonNull String question) {
            this.question = question;
            return this;
        }

        /**
         * Set the text for the Negative Button.
         * @param optionNegative of the Poll
         * @return this builder
         */
        public Builder setOptionNegative(@NonNull String optionNegative) {
            this.optionNegative = optionNegative;
            return this;
        }

        /**
         * Set the text for the Later Button.
         * @param optionNeutral of the Poll
         * @return this builder
         */
        public Builder setOptionNeutral(@NonNull String optionNeutral) {
            this.optionNeutral = optionNeutral;
            return this;
        }

        /**
         * Set the text for the Yes Button.
         * @param optionPositive of the Poll
         * @return this builder
         */
        public Builder setOptionPositive(@NonNull String optionPositive) {
            this.optionPositive = optionPositive;
            return this;
        }

        /**
         * Create the Poll.
         * @return a Poll
         */
        @NonNull
        public Poll build() {
            return new Poll(this);
        }
    }
}
