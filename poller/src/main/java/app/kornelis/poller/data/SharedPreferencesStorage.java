package app.kornelis.poller.data;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import app.kornelis.poller.model.State;

/**
 * @author Kornelis.
 *
 * Storage implementation using Shared Preferences.
 */
public class SharedPreferencesStorage implements StorageContract {

    private static final String POLL = "poll-preferences";
    private static final String STATUS = "poll-status";
    private static final String DAY = "poll-day";
    private static final String DAY_OFFSET = "poll-day-offset";
    private static final String TIMES = "poll-times";
    private static final String TIMES_OFFSET = "poll-times-offset";
    private static final String FORMAT = "yyyy-MM-dd HH:mm:ss";

    private final SharedPreferences sharedPreferences;
    private final SimpleDateFormat dateFormat;

    /**
     * Init the Shared Preferences Storage.
     * @param context from the Context: this or getContext().
     */
    public SharedPreferencesStorage(Context context) {
        this.sharedPreferences = context.getSharedPreferences(POLL, Context.MODE_PRIVATE);
        this.dateFormat = new SimpleDateFormat(FORMAT, Locale.getDefault());
    }

    @Override
    public void setState(@NonNull State state) {
        setStatus(state.status);
        setTimesLaunched(state.timesLaunched);

        String installDateTime = retrieveString(DAY);
        if (installDateTime.equals(DAY)) {
            setDayInstalled();
        }
    }

    @NonNull
    @Override
    public State getState() {
        State state = new State();
        state.status = getStatus();
        state.daysInstalled = getDaysInstalled();
        state.timesLaunched = getTimesLaunched();

        int daysOffset = getAskDayOffset();
        if (daysOffset > 0) {
            state.pollAfterDay = state.pollAfterDay + daysOffset;
        }
        int timesOffset = getAskTimeOffset();
        if (timesOffset > 0) {
            state.pollAfterLaunchTimes = state.pollAfterLaunchTimes + timesOffset;
        }

        return state;
    }

    @Override
    public void setStatus(int status) {
        storeInt(STATUS, status);
    }

    @Override
    public int getStatus() {
        return retrieveInt(STATUS);
    }

    @Override
    public void setDayInstalled() {
        storeString(DAY, dateFormat.format(getNow()));
    }

    @Override
    public int getDaysInstalled() {
        String installDateTime = retrieveString(DAY);
        if (!installDateTime.equals(DAY)) { // is installed before.
            Date now = getNow();
            try {
                Date firstRun = dateFormat.parse(installDateTime);
                long duration = now.getTime() - firstRun.getTime();
                return (int) TimeUnit.DAYS.convert(duration, TimeUnit.MILLISECONDS);

            } catch (Exception e) {
                // nothing, don't care.
            }
        }

        return 0;
    }

    @Override
    public void addAksDayOffset(int extraDays) {
        int days = retrieveInt(DAY_OFFSET);
        storeInt(DAY_OFFSET, days + extraDays);
    }

    @Override
    public int getAskDayOffset() {
        return retrieveInt(DAY_OFFSET);
    }

    @Override
    public void setTimesLaunched(int times) {
        storeInt(TIMES, times);
    }

    @Override
    public int getTimesLaunched() {
        return retrieveInt(TIMES);
    }

    @Override
    public void addAskTimeOffset(int extraTimes) {
        int times = retrieveInt(TIMES_OFFSET);
        storeInt(TIMES_OFFSET, times + extraTimes);
    }

    @Override
    public int getAskTimeOffset() {
        return retrieveInt(TIMES_OFFSET);
    }

    /**
     * Store an Int in the Shared Preferences.
     * @param key so we can find it later in the Shared preferences.
     * @param value of the int.
     */
    private void storeInt(@NonNull String key, int value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * Get the value from the Shared Preferences.
     * @param key to get the value.
     * @return value for this key, returns a 0 if not found.
     */
    private int retrieveInt(@NonNull String key) {
        return sharedPreferences.getInt(key, 0);
    }

    /**
     * Store a String in the Shared Preferences.
     * @param key so we can find it later in the Shared preferences.
     * @param value of the String.
     */
    private void storeString(@NonNull String key, @NonNull String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Retrieve the (hopefully) previously stored value.
     * @param key to get the value.
     * @return value for this key, returns the key if not found.
     */
    private String retrieveString(@NonNull String key) {
        return sharedPreferences.getString(key, key);
    }

    /**
     * Date time point now.
     * @return now
     */
    @NonNull
    private Date getNow() {
        return Calendar.getInstance().getTime();
    }
}
