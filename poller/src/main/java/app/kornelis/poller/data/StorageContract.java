package app.kornelis.poller.data;

import androidx.annotation.NonNull;

import app.kornelis.poller.constants.States;
import app.kornelis.poller.model.State;

/**
 * @author Kornelis.
 *
 * How ever someone wants to store these values, share preferences, a database or even a server, it must use this interface.
 */
public interface StorageContract {

    /**
     * Store the State in one go.
     * @param state to store.
     */
    void setState(@NonNull State state);

    /**
     * Get the last stored state.
     */
    @NonNull
    State getState();

    /**
     * Store the status for us.
     * @param status #State.PollStatus.
     */
    void setStatus(@States.PollStates int status);

    /**
     * @return #setStatus
     */
    @States.PollStates
    int getStatus();

    void setDayInstalled();

    int getDaysInstalled();

    void addAksDayOffset(int extraDays);

    int getAskDayOffset();

    void setTimesLaunched(int times);

    int getTimesLaunched();

    void addAskTimeOffset(int extraTimes);

    int getAskTimeOffset();
}
