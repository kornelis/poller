package app.kornelis.poller;

import android.content.Context;
import androidx.annotation.NonNull;
import app.kornelis.poller.constants.States;
import app.kornelis.poller.data.SharedPreferencesStorage;
import app.kornelis.poller.data.StorageContract;
import app.kornelis.poller.model.Poll;
import app.kornelis.poller.view.DialogQuestion;
import app.kornelis.poller.view.QuestionContract;

/**
 * @author Kornelis, February 2021.
 * Dialog Poller is cleaner then "AlertDialog SharedPreferences Rating Poller".
 * View:    AlertDialog.
 * Storage: SharedPreferences.
 * Action:  Google Play Store Rating.
 */
public class DialogPoller extends BasePoller {

    public void onCreate(@NonNull Context context) {
        Poll poll = new Poll.Builder(context.getString(R.string.poller_rating_title))
                .setQuestion(context.getString(R.string.poller_rating_question))
                .setOptionNegative(context.getString(R.string.poller_option_no))
                .setOptionNeutral(context.getString(R.string.poller_option_later))
                .setOptionPositive(context.getString(R.string.poller_option_yes))
                .build();

        QuestionContract question = new DialogQuestion(context, this, poll);
        StorageContract storage = new SharedPreferencesStorage(context);

        super.onCreate(question, storage);
    }

    @Override
    public void onNegative() {
        storage.setStatus(States.NO);
    }

    @Override
    public void onNeutral() {
        askLater();
    }

    @Override
    public void onPositive() {
        // Todo("Point user to where they can leave a review")
    }

    @Override
    public void onCancel() {
        askLater();
    }

    private void askLater() {
        storage.setStatus(States.LATER);
        storage.addAksDayOffset(state.pollDelayDay);
        storage.addAskTimeOffset(state.pollDelayLaunchTimes);
    }
}
