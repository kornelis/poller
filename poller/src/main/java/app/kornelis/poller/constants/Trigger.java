package app.kornelis.poller.constants;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Kornelis.
 *
 * Types of Trigger we can use to start polling.
 */
public final class Trigger {

    @IntDef({
            TIME_AND_LAUNCH,
            TIME_OR_LAUNCH,
            TIME,
            LAUNCH})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TriggerOn {}

    /**
     * Start asking the user if Days AND Times Launched are both met.
     */
    public static final int TIME_AND_LAUNCH = 0;

    /**
     * Start asking the user if Days OR Times Launched are met.
     */
    public static final int TIME_OR_LAUNCH = 1;

    /**
     * Start asking the user if Days is met.
     */
    public static final int TIME = 2;

    /**
     * Start asking the user if Times Launched is met.
     */
    public static final int LAUNCH = 3;
}
