package app.kornelis.poller.constants;

import androidx.annotation.IntDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Kornelis.
 *
 * States of Polling we can use to determen what to do.
 */
public final class States {

    @IntDef({
            NOT_ASKED,
            LATER,
            NO,
            DONE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface PollStates {}

    /**
     * We have not asked the user.
     */
    public static final int NOT_ASKED = 20;

    /**
     * We have asked the user, and the user said later.
     */
    public static final int LATER = 21;

    /**
     * We have asked the user, and the user said no.
     */
    public static final int NO = 22;

    /**
     * We have asked the user, and the user has done its thing.
     */
    public static final int DONE = 23;
}
