package app.kornelis.poller;

import androidx.annotation.NonNull;

import app.kornelis.poller.data.StorageContract;
import app.kornelis.poller.model.State;
import app.kornelis.poller.view.QuestionContract;

import static app.kornelis.poller.constants.States.*;
import static app.kornelis.poller.constants.Trigger.*;

/**
 * @author Kornelis.
 *
 * Base Poller handles the logic of polling.
 */
public abstract class BasePoller implements PollFeedback {

    QuestionContract question;
    StorageContract storage;
    State state;

    /**
     * Setting up the interfaces and getting data from them, if available.
     * @param question what the user interacts with.
     * @param storage to store and retrieve the values.
     */
    public void onCreate(@NonNull QuestionContract question, @NonNull StorageContract storage) {
        this.question = question;
        this.storage = storage;
        this.state = storage.getState();
    }

    /**
     * Let's see if we should ask the user something.
     */
    public void onResume() {
        // What are the results from the past?
        if (state == null || state.status == NO || state.status == DONE) {
            return;
        }

        // Is it time to start Polling (yet)?
        boolean enoughDays = state.daysInstalled >= state.pollAfterDay;
        boolean enoughLaunches = state.timesLaunched >= state.pollAfterLaunchTimes;

        if (!enoughDays && !enoughLaunches) {
            return;
        }

        boolean canStart = state.trigger == TIME_AND_LAUNCH && enoughDays && enoughLaunches;
        if (!canStart) canStart = state.trigger == TIME_OR_LAUNCH;
        if (!canStart) canStart = state.trigger == TIME && enoughDays;
        if (!canStart) canStart = state.trigger == LAUNCH && enoughLaunches;

        if (canStart) {
            question.setup();
            question.show();
        }
    }

    /**
     * Clear out, stash it to safety, the thing is going down.
     */
    public void onPause() {
        storage.setState(state);
    }

    /**
     * Get the question, by its contract.
     * @return the question set in the constructor.
     */
    @NonNull
    public QuestionContract getQuestion() {
        return this.question;
    }

    /**
     * Get the storage, by its contract.
     * @return the storage set in the constructor.
     */
    @NonNull
    public StorageContract getStorage() {
        return this.storage;
    }

    /**
     * Getter for the current Poll state.
     * @return state
     */
    @NonNull
    public State getState() {
        return state;
    }

    /**
     * Set the State.
     * Not sure why you'd want it, but here it is.
     * @param state of Polling.
     * @return this
     */
    public BasePoller setState(@NonNull State state) {
        this.state = state;
        return this;
    }
}
