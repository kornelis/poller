package app.kornelis.poller.view;

import android.content.Context;
import android.content.DialogInterface;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import app.kornelis.poller.PollFeedback;
import app.kornelis.poller.model.Poll;

/**
 * @author Kornelis.
 * Create a (Alert) Dialog to Poll the user.
 * If the user clicks somewhere else we handle that as a neutral button click.
 */
public class DialogQuestion implements QuestionContract, DialogInterface.OnClickListener, DialogInterface.OnCancelListener {

    private final Context context;
    private final PollFeedback feedback;
    private final Poll poll;
    private AlertDialog dialog;

    /**
     * Setting up the (Alert) Dialog.
     * @param context needed to inflate the Dialog.
     * @param feedback needed to do something with the what the user has selected.
     * @param poll with the needed poll details.
     */
    public DialogQuestion(@NonNull final Context context, @NonNull final PollFeedback feedback, @NonNull final Poll poll) {
        this.context = context;
        this.feedback = feedback;
        this.poll = poll;
    }

    @Override
    public void setup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(poll.title)
                .setMessage(poll.question)
                .setNegativeButton(poll.optionNegative, this)
                .setNeutralButton(poll.optionNeutral, this)
                .setPositiveButton(poll.optionPositive, this)
                .setCancelable(true)
                .setOnCancelListener(this);
        // Create the AlertDialog object.
        dialog = builder.create();
    }

    @Override
    public void show() {
        if (dialog != null) {
            dialog.show();
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                feedback.onPositive();
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                feedback.onNegative();
                break;
            case DialogInterface.BUTTON_NEUTRAL:
                feedback.onNeutral();
                break;
            default:
                feedback.onCancel();
                break;
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        feedback.onCancel();
    }
}
