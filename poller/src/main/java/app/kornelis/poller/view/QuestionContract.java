package app.kornelis.poller.view;

/**
 * @author Kornelis.
 *
 * How ever we show stuff to the user, AlertDialogs for example, it must use this interface.
 */
public interface QuestionContract {

    /**
     * Setup / init / build what ever you need.
     * Gets called before show().
     */
    void setup();

    /**
     * Finish creating the thing visible to the User.
     */
    void show();
}
