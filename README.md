# Poller Library

A Poller is one who registers voters or conducts a poll. Also (computing), a task or process that periodically checks for a condition being satisfied.


### Why Poller?

Getting feedback or a review can be very valuable for developing an App. So here is an (yet another) Android Library that does just that.


### A simple to use Android Library

Setup the libary, customise the values and nudge the user for something.


### Extensible

Want to use an other storage system? Don't like the AlertDialog provided? Provide your own by extending the provided interfaces.


### The Poll

A Poll has 4 parts:

 1. When to ask the Question.
 1. The Question that is visible to the user.
 1. Answer on that Question.
 1. Action to an Answer.


### User Experiance

 1. Dialog with: "Do you like this App?" -> "Ask Later", "No" and "Yes"
 1. Like: Ask later -> Add a number of days / app opened to ask later.
 1. Feedback: No Dialog: "Would you like to provide feedback to the developer?" -> "Ask Later", "No" and "Yes".
 1. Review Dialog: "Would you like give a review for this app?" -> "Ask Later", "No" and "Yes".
 1. No -> User does not want to give feedback or leave a review. Don't ask again.
 1. Feedback Yes -> Default action is to send an email. Function can be overridden.
 1. Review Yes -> Send to Google In App Review. Function can be overridden do adjust to what you want.
